# TP7 Conception logicielle : Examen

## Modalités

### Contexte

Les modalités de cet examen sont proches de celles d'un compte-rendu de TP.  
Il vous est demandé de réaliser une application répondant à la spécification donnée.

### Calendrier

L'évaluation commence lors de la séance du lundi 19 avril matin. Vous y disposez de 3 heures pour commencer le projet.  
Le rendu final est exigé pour le **mercredi 4 mai à 20h**. Il n'est pas attendu une grande quantité de travail (environ 4 heures) entre ces 2 dates.  

### Rendu attendu

Le rendu attendu est un dépôt git **public**, hébergé sur **votre compte personnel** sur la plateforme de votre choix (par défaut, gitlab.com mais vous êtes libres de choisir une plateforme concurrente tel que github ou bitbucket).  
Le lien de votre dépôt git devra être envoyé par mail à `olivier.levitt@gmail.com` avant la deadline de mercredi 4 mai 20h.

### Critères d'évaluation

Les critères d'évaluation sont les suivants :

- Un dépôt git bien entretenu
  - Le dépôt est public
  - Les fichiers versionnés respectent les bonnes pratiques (texte brut, uniquement les sources pas les produits ...)
  - L'historique permet de retracer les différentes étapes du développement (pas un simple unique commit)
  - Bonus : utilisation opportune des branches, issues et merge requests
- Du python industriel
  - Les dépendances sont clairement listées et installables en une commande
  - Le projet est lançable avec le minimum d'opérations manuelles
- Des applicatifs fonctionnels:
  - Un webservice accessible via HTTP, après démarrage
  - Un client pouvant accéder au webservice développé
  - Mise en place du scénario client
  - Bonus: De la gestion de cas limites, documentée dans le projet via commentaires.
- De la documentation
  - L'objectif, l'organisation et le fonctionnement du code est décrit succintement dans une documentation intégrée proprement au dépôt git
  - Une partie "quickstart" (démarrage rapide) est présente dans la documentation pour indiquer les quelques commandes standards pouvant être utilisées pour lancer le code
  - Un schéma simple d'architecture macro de l'application (rappel : des systèmes comme [Mermaid](https://docs.gitlab.com/ee/user/markdown.html#mermaid) intégrés à Gitlab permettent de réaliser des schémas à partir de code directement sur le repo git)
- De la portabilité
  - Les éventuels paramètres de configuration du projet sont externalisés et surchargeables  
  - Le projet n'a pas d'adhérence à une machine en particulier (liens de fichiers en dur, adhérence à un système d'exploitation particulier ...)
- De la qualité
  - Au moins un test unitaire est présent
  - La façon de lancer les tests est documentée
- De l'automatisation
  - Bonus : un pipeline s'exécute à chaque push et lance les tests unitaires présents dans le projet

### Conditions de l'examen

Les conditions de cet examen se veulent proches de vos futures conditions de travail professionnelles.  
Ainsi, vous avez accès à toutes les ressources que vous souhaitez (internet, stackoverflow, forums, chats). De même, la réutilisation de code est permise, tant que vous en respectez la license d'utilisation.  
Vous êtes aussi libres de choisir votre environnement de travail. Il vous est ainsi par exemple toujours possible d'accéder aux services en ligne de la plateforme SSPCloud : [Datalab](https://datalab.sspcloud.fr) mais vous pouvez tout à fait travailler sur votre environnement local ou sur tout autre environnement qui vous conviendrait.
Enfin, Les professeurs sont mobilisables, dans la limite du raisonnable, pendant toute la période de l'examen. Evidemment, comme pour toute demande de débug, il est demandé de joindre, pendant et hors séance de TP, le code correspondant (lien vers le dépôt git à jour), le résultat attendu et l'erreur rencontrée (message d'erreur + `stacktrace`).

> Le sujet est délibérément proposé sous la forme d'une spécification d'un besoin, comme il en serait dans un contexte professionnel

## Sujet

Le sujet proposé pour cet épreuve est la réalisation d'une API musicale, qui: 

- Permet de proposer de manière aléatoire une chanson, pour un artiste donné en entrée  
- Contrôle l'état de santé des api cible

Ce sujet s'appuie sur l'utilisation de 2 API externes : 
    
- AudioDB : https://www.theaudiodb.com/api_guide.php
> Grand webservice servant de banque de données sur les informations sur des artistes, albums, titres
- Lyricsovh : https://lyricsovh.docs.apiary.io/
> Permet pour un artiste, et un titre de trouver les paroles associées au titre

Pour réaliser cela, on attend donc :  
    - l'implémentation d'un web-service dédié  
    - l'implémentation d'un client pour la réalisation du scénario

## Tour d'horizon des API proposées
- AudioDB propose une large quantité d'informations sur les musiques.
    - A partir d'un artiste récupéré par son nom
https://www.theaudiodb.com/api/v1/json/2/search.php?s=rick%20astley
    - On peut accéder aux albums de l'artiste
    https://theaudiodb.com/api/v1/json/2/album.php?i=112884
    - Puis récupérer les morceaux de cet album
https://theaudiodb.com/api/v1/json/2/track.php?m=2121296

- LyricsOvh propose un unique endpoint a la racine

> https://api.lyrics.ovh/v1/{artist}/{title}

exemple:
https://api.lyrics.ovh/v1/rick%20astley/Never%20gonna%20give%20you%20up

## Spécificité du schéma d'architecture
Pour réaliser le schéma d'architecture, nous vous invitons a utiliser au possible une solution qui s'intègre au markdown. 

**Mermaid**: https://mermaid-js.github.io/mermaid/#/
```mermaid
flowchart LR
    A --> B --> C --> D
```

- Solution  qui permet, a partir d'une syntaxe simple, de générer des diagrammes :
    - C'est intégré par défaut sur gitlab -> https://docs.gitlab.com/ee/user/markdown.html#mermaid
    - Exemple de Gist sur Github : https://gist.github.com/andreia/ef2fd292c6fdadb6a927b975c300e532

> Pourquoi ? Utilisation sinon de formats d'image pas trop compatible avec la logique de git.

## Webservice / API
    
L'application cible sera constituée d'une API qui exposera les endpoints suivant :  

- Une ressource en **HTTP GET** sur `/` qui teste la bonne configuration et la disponibilité des services dont dépend l'API. Ce endpoint devra répondre des codes HTTP sémantiquement corrects en fonction de la situation.

- Une ressource en **HTTP GET** sur `/random/{artist_name}` qui renvoie, pour un string correspondant au nom d'un artiste donné en entrée, les informations sur une musique de cet artiste au hasard :  
```json
{
    "artist":"rick astley",
    "title":"Never Gonna Give You Up",
    "suggested_youtube_url":"http://www.youtube.com/watch?v=dQw4w9WgXcQ",
    "lyrics":"We're no strangers to love, You know the rules and so do I"
}
```  

## Scénario / Client

Rudy souhaite réaliser une soirée karaoké avec des amis. La soirée commence bientôt et la playlist n'est pas prête ! En plus, Rudy a des goûts musicaux bien précis, il n'aime que certains artistes. Rudy souhaite donc générer automatiquement une playlist de musiques avec lyrics uniquement composée de musiques de ses artistes préférés.  
Rudy ayant fait une école nationale de statistiques et d'analyse de l'information, il a préparé un fichier JSON contenant le nom de ses artistes préférés. 

```json
[{
    "artiste": "daft punk",
    "note": 18
},
{
    "artiste":"gloria gaynor",
    "note": 10
},
{
    "artiste":"boney m",
    "note": 5
},
{
    "artiste":"oasis",
    "note": 20
},
{
    "artiste":"Bob Marley",
    "note": 10
},
{
    "artiste":"Tryo",
    "note": 20
},
{
    "artiste":"Technotronic"
    "note": 10
},
{
    "artiste":"dire straits",
    "note": 16
}]
```
> rudy.json

Il souhaite maintenant générer une playlist de 20 chansons à partir de ce fichier.   
Réalisez un client python qui génère une playlist à partir d'un fichier au format "rudy".
